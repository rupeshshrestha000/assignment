from tabulate import tabulate

# Title
print("""
**********************************
        Stock Manager
**********************************
""")

# ==============Defining list to store input data==========================
product_list = []
# =====================loop for product entry========================
selection = "y"

while selection == "y":

    # =================Enter Product Lists======================
    product_name = input("Product name: ")
    stock_level = int(input("Stock Level: "))


    # Check validation if there is data or not
    if product_name or stock_level == "":
        product_list.append([product_name, stock_level])

        # =====================Print confrimation========================
        print("\n" + product_name + " has been entered with a stock of " + "stock_level" + " products")

    # =====================add more product confrimation===================
    selection = input(" \n Do you wish to enter another record? (Y/N)").lower()


# =====================Printing in tabular form (tabulate 0.8.5 Library used)======================
print(tabulate(product_list, headers=['Products', 'Stock']))


