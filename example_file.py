from tabulate import tabulate

product_list= []

def add_product(obj):
    product_name = input("Product Name: ")
    product_list.append([product_name])
    print(product_name + "was added to the list of products.")

def increase_stock(obj):
    print("Enter a product from the following list:")
    available_items = ', '.join(product_list)
    print(available_items)
    written_product = input("Product Name: ")

    if written_product != product_list:
        print("*** Error: Product not listed")

    else:
        stock_level = input("Stock Level: ")
        product_list.append([written_product, stock_level])
        print(written_product + "has has been updated to a stock of" + stock_level + "products.")

def list_product(obj):
    print(tabulate(product_list, headers=['Products', 'Stock']))


def main():
    run = True
    add_product = 'a'
    increase_stock = 'i'
    list_product = 'l'
    choice = input("Your Choice")
    while run:
        if choice == "a":
            add_product()

