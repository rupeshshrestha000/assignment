from tabulate import tabulate

# Title
print("""
**********************************
        Stock Manager
**********************************
""")

# ==============Defining list to store input data==========================
product_list = []
total = 0
# =====================loop for product entry========================
selection = "y"

while (selection == "y" or "Y"):

    # =================Enter Product Lists======================
    product_name = input("Product name: ")
    stock_level = int(input("Stock Level: "))
    # total = total + stock_level

    # Check validation if there is data or not
    if product_name or stock_level == "":
        product_list.append([product_name, stock_level])
        # =====================Print confrimation========================
        print("\n" + product_name + " has been entered with a stock of " + "stock_level" + " products")

    # =====================add more product confrimation===================
    # selection = input(" \n Do you wish to enter another record? (Y/N)").lower()

    question = "Do you wish to enter another record? (Y/N): "
    choice = input(question)
    while choice not in ['y', 'Y', 'n', 'N']:
        print('Invalid choice')
        choice = input(question)
    selection = choice in ['y', 'Y']

# =====================Printing in tabular form (tabulate 0.8.5 Library used)======================
print(tabulate(product_list, headers=['Products', 'Stock', 'Total']))


